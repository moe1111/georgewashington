package com.intelligentmobiledesign.georgewashington;

public class Item {
	public String id;
	public String content;
	int icon;
	int image1;
	String txt1;
	String txt2;
	String txt3;
	String caption;

	public Item(String id, String content, int icon, int image1, String txt1,
			String txt2, String txt3, String caption) {
		this.id = id;
		this.content = content;
		this.icon = icon;
		this.txt1 = txt1;
		this.txt2 = txt2;
		this.txt3 = txt3;
		this.image1 = image1;
		this.caption = caption;
	}

	@Override
	public String toString() {
		return content;
	}

	public String getTitle() {
		return content;
	}

	public String getCaption() {
		return caption;
	}

	public String getInfo1() {
		return txt1;
	}

	public int getImage1() {
		return image1;
	}

	public CharSequence getInfo2() {
		return txt2;
	}

	public CharSequence getInfo3() {
		return txt3;
	}

}