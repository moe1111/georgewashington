package com.intelligentmobiledesign.georgewashington;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class PictureDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The content this fragment is presenting.
	 */
	private Item mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public PictureDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			mItem = Content.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_picture, container,
				false);

		// Show the dummy content as text in a TextView.
		if (mItem != null) {
			Log.i(getClass().getName(), "Initializing picture fragment for "
					+ mItem.getTitle());
			((ImageView) rootView.findViewById(R.id.img1))
					.setImageResource(mItem.getImage1());
			Toast.makeText(getActivity().getBaseContext(), mItem.getCaption(),
					Toast.LENGTH_LONG).show();
		}

		return rootView;
	}

}
