package com.intelligentmobiledesign.georgewashington;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 */
public class Content {

	/**
	 * An array of items.
	 */
	public List<Item> items = new ArrayList<Item>();

	public Content(Context context) {
		super();

		addItem(new Item("1", "Early Life", R.drawable.crib,
				R.drawable.early_life_1, context.getResources().getString(
						R.string.info_early_life), context.getResources()
						.getString(R.string.info_early_life_2), context
						.getResources().getString(R.string.info_early_life_3),
				context.getResources().getString(R.string.caption_early_life)));
		addItem(new Item("2", "Education", R.drawable.crib,
				R.drawable.education_1, context.getResources().getString(
						R.string.info_education), context.getResources()
						.getString(R.string.info_education_2), context
						.getResources().getString(R.string.info_education_3),
				context.getResources().getString(R.string.caption_education)));
		addItem(new Item("3", "French and Indian War", R.drawable.indian,
				R.drawable.french_and_indian_war_1, context.getResources()
						.getString(R.string.info_french_and_indian_war),
				context.getResources().getString(
						R.string.info_french_and_indian_war_2), context
						.getResources().getString(
								R.string.info_french_and_indian_war_3), context
						.getResources().getString(
								R.string.caption_french_and_indian_war)));
		addItem(new Item("4", "Revolution", R.drawable.musket,
				R.drawable.revolution_1, context.getResources().getString(
						R.string.info_revolution), context.getResources()
						.getString(R.string.info_revolution_2), context
						.getResources().getString(R.string.info_revolution_3),
				context.getResources().getString(R.string.caption_revolution)));
		addItem(new Item("5", "Articles of Confederation", R.drawable.crib,
				R.drawable.confederation_1, context.getResources().getString(
						R.string.info_confederation), context.getResources()
						.getString(R.string.info_confederation_2), context
						.getResources()
						.getString(R.string.info_confederation_3), context
						.getResources().getString(
								R.string.caption_confederation)));
		addItem(new Item("6", "Presidency", R.drawable.crib,
				R.drawable.presidency_1, context.getResources().getString(
						R.string.info_presidency), context.getResources()
						.getString(R.string.info_presidency_2), context
						.getResources().getString(R.string.info_presidency_3),
				context.getResources().getString(R.string.caption_presidency)));
		addItem(new Item("7", "Legacy", R.drawable.crib, R.drawable.legacy_1,
				context.getResources().getString(R.string.info_legacy), context
						.getResources().getString(R.string.info_legacy_2),
				context.getResources().getString(R.string.info_legacy_3),
				context.getResources().getString(R.string.caption_legacy)));

	}

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, Item> ITEM_MAP = new HashMap<String, Item>();

	private void addItem(Item item) {
		items.add(item);
		ITEM_MAP.put(item.id, item);
	}

	public Item getItem(int id) {
		return ITEM_MAP.get(id);
	}
}
