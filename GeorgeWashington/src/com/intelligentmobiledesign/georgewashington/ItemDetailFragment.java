package com.intelligentmobiledesign.georgewashington;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
public class ItemDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The content this fragment is presenting.
	 */
	private Item mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			mItem = Content.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);

		// Show the dummy content as text in a TextView.
		if (mItem != null) {
			Log.i(getClass().getName(),
					"Initializing fragment for " + mItem.getTitle());
			Typeface tf = Typeface.createFromAsset(getActivity()
					.getBaseContext().getAssets(), "fonts/old.ttf");
			((TextView) rootView.findViewById(R.id.txt_info_2)).setTypeface(tf);
			((TextView) rootView.findViewById(R.id.title)).setText(mItem
					.getTitle());
			((TextView) rootView.findViewById(R.id.txt_info)).setText(mItem
					.getInfo1());
			((ImageView) rootView.findViewById(R.id.img1))
					.setImageResource(mItem.getImage1());
			((TextView) rootView.findViewById(R.id.txt_info_2)).setText(mItem
					.getInfo2());
			((TextView) rootView.findViewById(R.id.txt_info_3)).setText(mItem
					.getInfo3());
		}

		return rootView;
	}

}
