package com.intelligentmobiledesign.georgewashington;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * An activity representing a list of Items. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link ItemDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ItemListFragment} and the item details (if present) is a
 * {@link ItemDetailFragment}.
 * <p>
 * This activity also implements the required {@link ItemListFragment.Callbacks}
 * interface to listen for item selections.
 */
public class ItemListActivity extends FragmentActivity implements
		ItemListFragment.Callbacks {

	private static final String KEY_TEXT_VALUE = "currentId";
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;
	private String currentId = "1";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_list);
		if (savedInstanceState != null) {
			currentId = savedInstanceState.getString(KEY_TEXT_VALUE);
		}
		if (findViewById(R.id.item_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			// ((ItemListFragment) getFragmentManager().findFragmentById(
			// R.id.item_list)).setActivateOnItemClick(true);
			onItemSelected(currentId);
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_TEXT_VALUE, currentId);
	}

	/**
	 * Callback method from {@link ItemListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		currentId = id;
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(ItemDetailFragment.ARG_ITEM_ID, id);
			ItemDetailFragment fragment = new ItemDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.item_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, ItemDetailActivity.class);
			detailIntent.putExtra(ItemDetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
		}
	}

	public void onImageClick(View v) {
		Log.i(getClass().getName(), "picture was clicked.");
		Bundle arguments = new Bundle();
		arguments.putString(ItemDetailFragment.ARG_ITEM_ID, currentId);
		PictureDetailFragment fragment = new PictureDetailFragment();
		fragment.setArguments(arguments);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.item_detail_container, fragment).commit();

	}

	public void onBigImageClick(View v) {
		Log.i(getClass().getName(), "big picture was clicked.");
		Bundle arguments = new Bundle();
		arguments.putString(ItemDetailFragment.ARG_ITEM_ID, currentId);
		ItemDetailFragment fragment = new ItemDetailFragment();
		fragment.setArguments(arguments);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.item_detail_container, fragment).commit();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	/* Respond to menu actions. */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_about_us:
			startActivity(new Intent(this, ImdActivity.class));
			return true;
		case R.id.action_feedback:
			collectFeedback();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void collectFeedback() {
		Intent Email = new Intent(Intent.ACTION_SEND);
		Email.setType("text/email");
		Email.putExtra(Intent.EXTRA_EMAIL, new String[] { getResources()
				.getString(R.string.feedback_email) });
		Email.putExtra(Intent.EXTRA_SUBJECT,
				getResources().getString(R.string.feedback_subject));
		Email.putExtra(Intent.EXTRA_TEXT,
				getResources().getString(R.string.feedback_msg));
		startActivity(Intent.createChooser(Email,
				getResources().getString(R.string.feedback_title)));
	}

}
